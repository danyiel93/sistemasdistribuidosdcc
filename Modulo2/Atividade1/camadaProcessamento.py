#Importando bibliotecas
import time
import socket
import json
from _thread import *
import threading

lock_server = threading.Lock()

#Função que chama a camada de dados
def chamarDados(arquivo):
	sock = socket.socket() 
	sock.connect(('localhost', 5000))
	sock.send(str.encode(arquivo))
	msg = sock.recv(8192)  # argumento indica a qtde maxima de bytes da mensagem
	return str(msg,  encoding='utf-8')

# thread function
def threaded(novoSock):
	global listen
	while True:		
		# data received from client
		data = novoSock.recv(8192)
		
		if not data:
			print('Bye')
			break

		res = str(data,  encoding='utf-8')
		print(res)

		#chamo a camada de dados
		res = chamarDados(str(data,  encoding='utf-8'))
		# espero 1 segundo
		time.sleep(1)
		#Se a resposta é no_file é porque o arquivo não foi encontrado, se não ele pega o texto e faz a lista das ocorrencias
		if res == "No_File":
			novoSock.send(str.encode("Arquivo não encontrado"))
		else:
			palavras = {}
			for word in res.split():
				if word in palavras:
					palavras[word] = palavras[word]+1
				else:
					palavras[word] = 1
			# send back reversed string to client
			palavras = sorted(palavras.items(), key=lambda item: item[1], reverse=True)
			novoSock.send(str.encode(json.dumps(palavras[:10])))

	# connection closed
	novoSock.close()

HOST = ''    # '' possibilita acessar qualquer endereco alcancavel da maquina local
PORTA = 5001  # porta onde chegarao as mensagens para essa aplicacao

# cria um socket para comunicacao
sock = socket.socket() # valores default: socket.AF_INET, socket.SOCK_STREAM  

# vincula a interface e porta para comunicacao
sock.bind((HOST, PORTA))

# define o limite maximo de conexoes pendentes e coloca-se em modo de espera por conexao
sock.listen(5) 

listen = 0
print('Iniciando')
while True: 
	# aceita a primeira conexao da fila (chamada pode ser BLOQUEANTE)
	novoSock, endereco = sock.accept() # retorna um novo socket e o endereco do par conectado
	print ('Conectado com: ', endereco)

	listen = listen + 1
	#Inicia a thread da conexão
	start_new_thread(threaded, (novoSock,))

# fecha o socket principal
sock.close() 

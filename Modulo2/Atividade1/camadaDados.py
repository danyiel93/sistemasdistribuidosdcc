#Importando bibliotecas
import time
import socket
import os.path
from _thread import *
import threading

lock_server = threading.Lock()
# thread function
def threaded(novoSock):
	global listen
	while True:		
		# data received from client
		data = novoSock.recv(8192)
		if not data:
			print('Bye')
			break

		print(str(data,  encoding='utf-8'))
		# espero 1 segundo
		time.sleep(1)

		#Se acho o arquivo retorno o conteudo dele, se não mando a mensagem "No_File"
		if os.path.isfile(data):
			f = open(data, encoding="utf8")
			lines = f.readlines()
			arquivo = ""
			for line in lines:
				arquivo = arquivo + " " + line
			novoSock.send(str.encode(arquivo))
		else:
			print("File not exist")
			# send back reversed string to client
			novoSock.send(str.encode("No_File"))

	# connection closed
	novoSock.close()

HOST = ''    # '' possibilita acessar qualquer endereco alcancavel da maquina local
PORTA = 5000  # porta onde chegarao as mensagens para essa aplicacao

# cria um socket para comunicacao
sock = socket.socket() # valores default: socket.AF_INET, socket.SOCK_STREAM  

# vincula a interface e porta para comunicacao
sock.bind((HOST, PORTA))

# define o limite maximo de conexoes pendentes e coloca-se em modo de espera por conexao
sock.listen(5) 

listen = 0
print ('Iniciando')
while True: 
	# aceita a primeira conexao da fila (chamada pode ser BLOQUEANTE)
	novoSock, endereco = sock.accept() # retorna um novo socket e o endereco do par conectado
	print ('Conectado com: ', endereco)

	listen = listen + 1
	#Inicia a thread da conexão
	start_new_thread(threaded, (novoSock,))

# fecha o socket principal
sock.close() 

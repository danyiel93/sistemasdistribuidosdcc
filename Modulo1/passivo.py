# Exemplo basico socket (lado passivo)
import time
import socket

# import thread module
from _thread import *
import threading

lock_server = threading.Lock()
# thread function
def threaded(novoSock):
	global listen
	# if listen > 2:
	# 	novoSock.send(b"Server ocupado, espera porfavor")
	# 	lock_server.acquire()
	while True:		
		# data received from client
		data = novoSock.recv(1024)
		if not data:
			print('Bye')
			break

		print(str(data,  encoding='utf-8'))
		# espero 1 segundo
		time.sleep(1)
		# send back reversed string to client
		novoSock.send(data)

	# connection closed
	novoSock.close()
	# listen = listen - 1
	# if(listen <= 2): lock_server.release()

HOST = ''    # '' possibilita acessar qualquer endereco alcancavel da maquina local
PORTA = 5000  # porta onde chegarao as mensagens para essa aplicacao

# cria um socket para comunicacao
sock = socket.socket() # valores default: socket.AF_INET, socket.SOCK_STREAM  

# vincula a interface e porta para comunicacao
sock.bind((HOST, PORTA))

# define o limite maximo de conexoes pendentes e coloca-se em modo de espera por conexao
sock.listen(5) 

listen = 0
while True: 
	# aceita a primeira conexao da fila (chamada pode ser BLOQUEANTE)
	novoSock, endereco = sock.accept() # retorna um novo socket e o endereco do par conectado
	print ('Conectado com: ', endereco)

	listen = listen + 1
	start_new_thread(threaded, (novoSock,))
	# while True:
	# 	# depois de conectar-se, espera uma mensagem (chamada pode ser BLOQUEANTE))
	# 	msg = novoSock.recv(1024) # argumento indica a qtde maxima de dados
	# 	if not msg: break 
	# 	else: print(str(msg,  encoding='utf-8'))
	# 	# espero 1 segundo
	# 	time.sleep(1)
	# 	# envia mensagem de resposta
	# 	novoSock.send(msg) 

	# # fecha o socket da conexao
	# novoSock.close() 

# fecha o socket principal
sock.close() 

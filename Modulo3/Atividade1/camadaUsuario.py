#Importando bibliotecas
import os.path
import socket
import json

HOST = 'localhost' # maquina onde esta o par passivo
PORTA = 5001        # porta que o par passivo esta escutando

# cria socket
sock = socket.socket() # default: socket.AF_INET, socket.SOCK_STREAM 

# conecta-se com o par passivo
sock.connect((HOST, PORTA)) 
while True:
    mensagem = input('Digite seu arquivo (\'sair\' para finalizar):: ')

    if mensagem == "sair": break
    # envia uma mensagem para o par conectado
    sock.send(str.encode(mensagem))

    #espera a resposta do par conectado (chamada pode ser BLOQUEANTE)
    # argumento indica a qtde maxima de bytes da mensagem
    msg = sock.recv(8192)

    # imprime a mensagem recebida
    res = str(msg,  encoding='utf-8')
    if(res == "Arquivo não encontrado"):
        print("Resposta: " + res)
    else:
        print("Resposta: ")
        dicPalavras = json.loads(res)
        for key in dicPalavras:
            #print(key + '->' + str(dicPalavras[key]))
            print(key[0] + "->" + str(key[1]))
        


# encerra a conexao
sock.close() 


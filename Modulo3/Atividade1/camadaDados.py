#Importando bibliotecas
import time
import socket
import os.path
from _thread import *
import threading
import sys
import select

#lock para acesso do dicionario 'conexoes'
lock = threading.Lock()

def iniciaServidor():
	'''Cria um socket de servidor e o coloca em modo de espera por conexoes
	Saida: o socket criado'''
	# cria um socket para comunicacao
	sock = socket.socket()  # valores default: socket.AF_INET, socket.SOCK_STREAM

	# vincula a interface e porta para comunicacao
	sock.bind((HOST, PORTA))

	# coloca-se em modo de espera por conexoes
	sock.listen(5)

	# configura o socket para o modo nao-bloqueante
	sock.setblocking(1)

	return sock

# thread function
def threaded(novoSock):
	global conexoesAtivas
	while True:		
		# data received from client
		data = novoSock.recv(8192)
		if not data:
			print('Bye')
			break

		print(str(data,  encoding='utf-8'))
		# espero 1 segundo
		time.sleep(1)

		#Se acho o arquivo retorno o conteudo dele, se não mando a mensagem "No_File"
		if os.path.isfile(data):
			f = open(data, encoding="utf8")
			lines = f.readlines()
			arquivo = ""
			for line in lines:
				arquivo = arquivo + " " + line
			novoSock.send(str.encode(arquivo))
		else:
			print("File not exist")
			# send back reversed string to client
			novoSock.send(str.encode("No_File"))

	# connection closed
	novoSock.close()
	#Diminuindo a quantidade de conexões ativas
	lock.acquire()
	conexoesAtivas = conexoesAtivas - 1
	lock.release()


def comando(sock):
	while True:
		cmd = input('Digite seu comando (\'sair\' para finalizar, \'conexoes\' para ver o numero de conexoes): \n')
		if cmd == 'sair':
			if conexoesAtivas > 0:
				print('Existem Conexões abertas')
			else:
				# fecha o socket principal
				sock.close()
		elif cmd == 'conexoes':
			print(conexoesAtivas)
		else:
			print('Digitou ' + cmd)

HOST = ''    # '' possibilita acessar qualquer endereco alcancavel da maquina local
PORTA = 5000  # porta onde chegarao as mensagens para essa aplicacao

conexoesAtivas = 0

def main():
	global conexoesAtivas
	'''Inicializa e implementa o loop principal (infinito) do servidor'''
	
	sock = iniciaServidor()

	#define a lista de I/O de interesse (jah inclui a entrada padrao)
	entradas = [sock]
	print ('Iniciando')
	'''Criando a thread que va a ler do sys.stdin, pois sempre que executei o sys.stdin dentro do socket dava erro
	procurei o problema na internet e achei que, no windows, a chamada select.select só aceita sockets e não o sys.stind 
	https://stackoverflow.com/questions/25071008/python-sock-chat-client-problems-with-select-select-and-sys-stdin/25071260
	https://docs.python.org/3/library/select.html
	'''
	start_new_thread(comando, (sock,))
	while True: 

		#espera por qualquer entrada de interesse
		leitura, escrita, excecao = select.select(entradas, [], [])

		#tratar todas as entradas prontas
		for pronto in leitura:
			if pronto == sock:  # pedido novo de conexao
				#Se a ultima conexão foi fechada saio do programa
				if pronto._closed:
					sys.exit()
				else:
					# retorna um novo socket e o endereco do par conectado
					novoSock, endereco = sock.accept()
					print('Conectado com: ', endereco)
					#Aumentando a conexões ativas
					lock.acquire()
					conexoesAtivas = conexoesAtivas + 1
					lock.release()

					#Inicia a thread da conexão
					start_new_thread(threaded, (novoSock,))
			

main()

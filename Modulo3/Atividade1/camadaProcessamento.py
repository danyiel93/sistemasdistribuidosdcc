#Importando bibliotecas
import time
import socket
import json
from _thread import *
import threading
import sys
import select

#lock para acesso do dicionario 'conexoes'
lock = threading.Lock()
conexoesAtivas = 0
def iniciaServidor():
	'''Cria um socket de servidor e o coloca em modo de espera por conexoes
	Saida: o socket criado'''
	# cria um socket para comunicacao
	sock = socket.socket()  # valores default: socket.AF_INET, socket.SOCK_STREAM

	# vincula a interface e porta para comunicacao
	sock.bind((HOST, PORTA))

	# coloca-se em modo de espera por conexoes
	sock.listen(5)

	# configura o socket para o modo nao-bloqueante
	sock.setblocking(1)

	return sock

#Função que chama a camada de dados
def chamarDados(arquivo):
	sock = socket.socket() 
	sock.connect(('localhost', 5000))
	sock.send(str.encode(arquivo))
	msg = sock.recv(8192)  # argumento indica a qtde maxima de bytes da mensagem
	return str(msg,  encoding='utf-8')

# thread function
def threaded(novoSock):
	global conexoesAtivas
	while True:		
		# data received from client
		data = novoSock.recv(8192)
		
		if not data:
			print('Bye')
			break

		res = str(data,  encoding='utf-8')
		print(res)

		#chamo a camada de dados
		res = chamarDados(str(data,  encoding='utf-8'))
		# espero 1 segundo
		time.sleep(1)
		#Se a resposta é no_file é porque o arquivo não foi encontrado, se não ele pega o texto e faz a lista das ocorrencias
		if res == "No_File":
			novoSock.send(str.encode("Arquivo não encontrado"))
		else:
			palavras = {}
			for word in res.split():
				if word in palavras:
					palavras[word] = palavras[word]+1
				else:
					palavras[word] = 1
			# send back reversed string to client
			palavras = sorted(palavras.items(), key=lambda item: item[1], reverse=True)
			novoSock.send(str.encode(json.dumps(palavras[:10])))

	# connection closed
	novoSock.close()
	#Diminuindo a quantidade de conexões ativas
	lock.acquire()
	conexoesAtivas = conexoesAtivas - 1
	lock.release()


def comando(sock):
	while True:
		cmd = input(
			'Digite seu comando (\'sair\' para finalizar, \'conexoes\' para ver o numero de conexoes): \n')
		if cmd == 'sair':
			if conexoesAtivas > 0:
				print('Existem Conexões abertas')
			else:
				# fecha o socket principal
				sock.close()
		elif cmd == 'conexoes':
			print(conexoesAtivas)
		else:
			print('Digitou ' + cmd)


HOST = ''    # '' possibilita acessar qualquer endereco alcancavel da maquina local
PORTA = 5001  # porta onde chegarao as mensagens para essa aplicacao

def main():
	global conexoesAtivas
	'''Inicializa e implementa o loop principal (infinito) do servidor'''
	sock = iniciaServidor()
	
	#define a lista de I/O de interesse (jah inclui a entrada padrao)
	entradas = [sock]
	print('Iniciando')
	'''Criando a thread que va a ler do sys.stdin, pois sempre que executei o sys.stdin dentro do socket dava erro
	procurei o problema na internet e achei que, no windows, a chamada select.select só aceita sockets e não o sys.stind 
	https://stackoverflow.com/questions/25071008/python-sock-chat-client-problems-with-select-select-and-sys-stdin/25071260
	https://docs.python.org/3/library/select.html
	'''
	start_new_thread(comando, (sock,))

	while True: 

		#espera por qualquer entrada de interesse
		leitura, escrita, excecao = select.select(entradas, [], [])

		#tratar todas as entradas prontas
		for pronto in leitura:
			if  pronto == sock:   # pedido novo de conexao
				#Se a conexão esta fechada acabo o programa
				if pronto._closed:
					sys.exit()
				else:
					# retorna um novo socket e o endereco do par conectado
					novoSock, endereco = sock.accept()
					print('Conectado com: ', endereco)
					#Aumentando a conexões ativas
					lock.acquire()
					conexoesAtivas = conexoesAtivas + 1
					lock.release()

					#Inicia a thread da conexão
					start_new_thread(threaded, (novoSock,))

main()

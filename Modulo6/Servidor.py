#Importando bibliotecas
import time
import socket
import json
from _thread import *
import threading
import random
import hashlib


def iniciaServidor(host,porta):
	'''Cria um socket de servidor e o coloca em modo de espera por conexoes
	Saida: o socket criado'''
	# cria um socket para comunicacao
	sock = socket.socket()  # valores default: socket.AF_INET, socket.SOCK_STREAM

	# vincula a interface e porta para comunicacao
	sock.bind((host, porta))

	# coloca-se em modo de espera por conexoes
	sock.listen(5)

	# configura o socket para o modo nao-bloqueante
	sock.setblocking(1)

	return sock

class NoPeer:
    def __init__(self, number, host, porta, vizinhos, nosTotais,predecesor):
        self.number = number
        self.host = host
        self.porta = porta
        self.vizinhos = vizinhos     
        self.nosTotais = nosTotais
        self.successor = vizinhos[1]
        self.memoria = {}
        self.predecesor = predecesor
        # cria um socket para comunicacao
        self.socket = socket.socket()
        print(f'{self.number} - {self.vizinhos}\n')

    def criarSocket(self):
        # vincula a interface e porta para comunicacao
        self.socket.bind((self.host, self.porta))

        # coloca-se em modo de espera por conexoes
        self.socket.listen(5)

        # configura o socket para o modo nao-bloqueante
        self.socket.setblocking(1)

    def listen(self):
        print(f'{self.number} - Esperando conexão programa filho\n')
        # retorna um novo socket e o endereco do par conectado
        novoSock, endereco = self.socket.accept()
        print(f'{self.number} - Conectado com: ', endereco)
        firstmessage = f'{self.number} - Digite uma mensagem para o nó (\'help\' para ajuda) ou \'sair\' para fechar a conexão: '
        novoSock.send(str.encode(firstmessage))
        while True:
            # data received from client
            data = novoSock.recv(8192)
            if not data:
                print(f'{self.number} - Bye')
                break

            print(f'{self.number} - received: ',{str(data,  encoding='utf-8')})
            data = str(data,  encoding='utf-8')
            if data == 'sair':
                print('Bye')
                break
            # espero 1 segundo
            time.sleep(1)
            message = ''
            #Comandos do usuario
            if(data == "help"):
                message = f'comados:\ninserir: inserir nova chave/valor\nprocurar: retorna o valor para a chave procurada\ninfo: retorna a informação desse nó\n' + firstmessage
            elif(data == "info"):
                message = f'no: {self.number} - porta: {self.porta} - fingertable: {self.vizinhos}\n' + firstmessage
            elif(data == "procurar"):
                message = f'{self.number} - digite a chave que quer procurar\n'
                # send back reversed string to client
                novoSock.send(str.encode(message))
                data = novoSock.recv(8192)
                if not data: break
                data = str(data,  encoding='utf-8')
                message = self.procuraChave(data) + '\n' + firstmessage
            #Usado para a comunicação entre os nos
            elif(data == "procurarNaRede"):
                # send back reversed string to client
                novoSock.send(str.encode(' '))
                data = novoSock.recv(8192)
                data = str(data,  encoding='utf-8')
                message = self.procuraChave(data)
            elif(data == "inserir"):
                message = f'{self.number} - digite a chave que quer inserir\n'
                # send back reversed string to client
                novoSock.send(str.encode(message))
                data = novoSock.recv(8192)
                if not data: break
                chave = str(data,  encoding='utf-8')
                message = f'{self.number} - digite a valor que quer inserir\n'
                novoSock.send(str.encode(message))
                data = novoSock.recv(8192)
                if not data: break
                valor = str(data,  encoding='utf-8')
                message = self.inserirChave(chave, valor) + '\n' + firstmessage
            elif(data == "inserirNaRede"):
                # send back reversed string to client
                novoSock.send(str.encode(' '))
                data = novoSock.recv(8192)
                chave = str(data,  encoding='utf-8')
                novoSock.send(str.encode(' '))
                data = novoSock.recv(8192)
                valor = str(data,  encoding='utf-8')
                message = self.inserirChave(chave, valor)
            else:
                message = f'{self.number} - comando desconhecido\n' + firstmessage
            # send back reversed string to client
            novoSock.send(str.encode(message))
        novoSock.send(str.encode('bye'))
        # connection closed
        novoSock.close()

    def procuraChave(self,chave):
        idDestino = self.calculaIdDestino(chave)
        if idDestino <= self.number and idDestino > self.predecesor:
            return self.searchKey(chave)
        else:
            noSucessor = self.procurarSucessor(idDestino)
            return self.chamarNoMaisPerto(chave,noSucessor[1],'procurar')

    def inserirChave(self,chave,valor):
        idDestino = self.calculaIdDestino(chave)
        if idDestino <= self.number and idDestino > self.predecesor:
            return self.insertKey(chave,valor)
        else:
            noSucessor = self.procurarSucessor(idDestino)
            return self.chamarNoMaisPerto(chave, noSucessor[1],'inserir',valor)

    def calculaHash(self,chave):
        hash_object = hashlib.sha1(str.encode(chave))
        pbHash = hash_object.hexdigest()
        return pbHash

    def calculaIdDestino(self,chave):
        pbHash = self.calculaHash(chave)
        IdDestino = int(pbHash, 16) % self.nosTotais
        print(f'{self.number} - Nó Destino: {IdDestino}, Hash: {pbHash}')
        return IdDestino

    def procurarSucessor(self, idDestino):
        tempId = idDestino
        if(self.number > idDestino):
            tempId = tempId + 64
        tempSucessor = self.successor[0]
        if(self.number > tempSucessor):
            tempSucessor = tempSucessor + 64
        if tempId < tempSucessor and tempId > self.number:
            return self.successor
        else:
            NoPerto = self.NoMaisPerto(idDestino)
            return NoPerto

    def NoMaisPerto(self, idDestino):
        tempId = idDestino
        if(self.number > idDestino):
            tempId = tempId + 64
        for i in reversed(range(len(self.vizinhos))):
            tempVizinho = self.vizinhos[2**i][0]
            if(self.number > tempVizinho):
                tempVizinho = tempVizinho + 64
            if(tempVizinho > self.number and tempVizinho <= tempId):
                return self.vizinhos[2**i]

    def chamarNoMaisPerto(self, chave, porta, comando, valor = ''):
        # cria socket
        sock = socket.socket()  # default: socket.AF_INET, socket.SOCK_STREAM
        # conecta-se com o par passivo
        sock.connect(('localhost', porta))
        #espera a resposta do par conectado (chamada pode ser BLOQUEANTE)
        # argumento indica a qtde maxima de bytes da mensagem
        msg = sock.recv(1024)
        if(comando == 'procurar'):
            # envia uma mensagem para o par conectado
            sock.send(str.encode('procurarNaRede'))
            msg = sock.recv(1024)
            sock.send(str.encode(chave))
            msg = sock.recv(1024)
        if(comando == 'inserir'):
            # envia uma mensagem para o par conectado
            sock.send(str.encode('inserirNaRede'))
            msg = sock.recv(1024)
            sock.send(str.encode(chave))
            msg = sock.recv(1024)
            sock.send(str.encode(valor))
            msg = sock.recv(1024)
            # encerra a conexao
        sock.send(str.encode('sair'))
        sock.recv(1024)
        sock.close()
        return str(msg,  encoding='utf-8')

    def searchKey(self, chave):
        shaChave = self.calculaHash(chave)
        if(shaChave in self.memoria):
            return f'{self.number} - {self.memoria[shaChave]}'
        else:
            return f'{self.number} - Chave não achada'

    def insertKey(self,chave,valor):
        shaChave = self.calculaHash(chave)
        self.memoria[shaChave] = valor
        return f'{self.number} - Chave inserida'

def calulaFingerTable(nos,m,porta):    
    #Crio o fingertable de cada nó
    fingerTable = []
    maxNo = nos[-1]
    for i in range(len(nos)):
        saltoTable = {}
        #Crio as 6 posições do finger table
        k = i
        for j in range(m):
            salto = 2**j
            #Procuro o nó do seguinte salto para cada salto
            while 1:
                if((nos[i]+salto) > maxNo and k > 15):
                    salto = salto - 2**m
                if((nos[i]+salto) <= nos[k % 16]):
                    saltoTable[salto % (2**m)] = [nos[k % 16], porta+(k % 16)]
                    break
                else:
                    k = k+1
        fingerTable.append(saltoTable)
    return fingerTable
# thread function
def threaded(number,host,port,vizinhos,nosTotais,predecesor):
    no_temp = NoPeer(number, host, port, vizinhos, nosTotais, predecesor)
    no_temp.criarSocket()
    while True:
        no_temp.listen()

def main():
    host = 'localhost'       # '' possibilita acessar qualquer endereco alcancavel da maquina local
    portaServer = 5001  # porta onde chegarao as mensagens para essa aplicacao

    print('Iniciando')
    portaNo = portaServer

    nos = [0]
    #Ciro as posições do circulo de nos, m = 6
    m = 6
    nos = nos + random.sample(range(1, 2**m-1), 15)

    nos.sort()
    print(nos)
    fingerTable = calulaFingerTable(nos, m, portaNo+1)
    print('Criando circulo')
    for i in range(len(nos)):
        portaNo = portaNo+1
        #Inicia a thread da conexão
        start_new_thread(
            threaded, (nos[i], host, portaNo, fingerTable[i], 2**m, nos[i-1]))

    # espero 1 segundo para que a criação dos filhos termine
    time.sleep(1)
    sock = iniciaServidor(host,portaServer)    
    while True:
        print('Esperando conexao programa principal')
        # retorna um novo socket e o endereco do par conectado
        novoSock, endereco = sock.accept()
        print('Conectado com: ', endereco)
        firstmessage = 'Digite uma mensagem para o nó (\'help\' para ajuda) ou \'sair\' para fechar a conexão: '
        novoSock.send(str.encode(firstmessage))
        while True:
            # data received from client
            data = novoSock.recv(1024)
            if not data:
                print('Bye')
                break

            print('received: ',str(data,  encoding='utf-8'))
            data = str(data,  encoding='utf-8')
            if data == 'sair':
                print('Bye')
                break
            # espero 1 segundo
            time.sleep(1)
            message = ''
            if(data == "help"):
                message = message + f'comados:\nlista: lista os nós filhos e as portas\n' + firstmessage
            elif(data == "lista"):
                for i in range(len(nos)):
                    message = message + f'no: {nos[i]} - porta: {portaServer+i+1}\n'
                message = message + firstmessage
            else:
                message = 'comando desconhecido\n' + firstmessage
            # send back reversed string to client
            novoSock.send(str.encode(message))
        novoSock.send(str.encode('bye'))

main()

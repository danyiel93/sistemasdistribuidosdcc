# Exemplo basico socket (lado ativo)

import socket

HOST = 'localhost' # maquina onde esta o par passivo

while True:
    # cria socket
    sock = socket.socket()  # default: socket.AF_INET, socket.SOCK_STREAM

    mensagem = input('Digite uma porta para se plugar num nó (nó principal 5001) ou sair, para sair do programa:')    

    if mensagem == "sair": break

    # conecta-se com o par passivo
    sock.connect((HOST, int(mensagem))) 
    while True:        

        #espera a resposta do par conectado (chamada pode ser BLOQUEANTE)
        msg = sock.recv(1024) # argumento indica a qtde maxima de bytes da mensagem

        # imprime a mensagem recebida
        #print("Resposta:")

        mensagem = str(msg,  encoding='utf-8')
        if(mensagem == 'bye'): break
        entrada = input(mensagem)

        # envia uma mensagem para o par conectado
        sock.send(str.encode(entrada))

    # encerra a conexao
    sock.close()
